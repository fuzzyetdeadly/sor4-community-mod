# Streets of Rage 4: REIGNITED

*Take me* [*home*](../README.md)

Latest version: **1.0.0**

Download it [**HERE**](../assets/SOR4-Reignited-v1.0.0.zip)

## Release notes

This page contains the latest release notes for `Streets of Rage 4: REIGNITED` (previously known as the `Community mod`).

![Mind:](./../images/icons/icon_warning.png) We intend to try and keep versions scoped, so any feedback out of scope for a version will have to be backlogged until we manage to prioritize them.

## Quick links

### SOR4 main cast

* [Axel](#axel)
* [Blaze](#blaze)
* [Cherry](#cherry)
* [Floyd](#floyd)
* [Adam](#adam)

### SOR4 DLC cast

* [Max](#max)
* [Estel](#estel)
* [Shiva](#shiva)

## Highlights

The scope of changes for this version includes:

* Quality of life changes for the **AI behaviors**
* Re-balancing the **original SOR4 cast's vanilla move-sets** to improve their combo potential
* **Experimental changes to DLC characters**, to try and balance them a bit

One key thing is, we aim to tune to AI based on the adjusted AI behavior. The principle of the design approach we are taking to, is that the player should not be forced into playing with safe loops just to survive; and that players should be given ample time to read and respond to what the AI is attempting to do.

This is in attempt to address many scenarios in which the player would get punished in ways that it was impossible to have escaped.

## Terminology

Some shortened terms you may encounter in reading:

* **OTG**: On the ground

## Global

* AI behavior changed to be more aggressive
* Enemy movement speed reduced
* Mid-air punishment by regular enemies’ jabs removed (except Signals)
* Extra minions added to boss fights in V8 have been removed
* Cursed arcade machines removed
* Duplicate bosses in the original arcade machines have been removed

## Character changes - Main cast

### Axel

|Basic moves|Changes|
|---|---|
|Neutral jump|Hit-stop increased|
|Air-combo|2nd hit: launch height increased (airborne enemy)|
|Back attack|1st hit: Damage increased from 8 to 12|
||2nd hit: Damage decreased from 12 to 10|
|Charge attack|Ability to use several charge attacks in a row restored (v4 feature)|
|Back slam / Suplex|Launch height increased|
|Vanilla blitz (Grand upper)|Damage reverted to v7 (decreased) from 46 to 40|
||Juggle properties reverted to v7|
||Pushback decreased (standing enemy)|
||Pushback increased (airborne enemy)|
||Launch height increased (standing & airborne enemy)|

|Specials|Changes
|---|---|
|Vanilla defensive (Dragon Wing)|1st hit: Damage increased from 8 to 12|
||1st hit: Pushback decreased (standing enemy)|
||1st hit: Cancellable into Offensive Special|
||2nd hit: Launch height increased (airborne enemy)|
||3rd hit: Hitbox improved|
|Vanilla offensive (Dragon Smash)|Green Health cost increased from 14 to 16|
||Last hit: Ground Bounce added|
||Last hit: Launch height decreased|
||Last hit: Cancellable into Air Special|
|Vanilla air (Dragon Dive)|Hitbox improved|
||Last hit: Launch height increased|
||Cancellable into Back Attack, Dragon Wing and Dragon Smash in either direction (during the roll)|

### Blaze

|Basic moves|Changes|
|---|---|
|Auto-combo|Jab damage increased from 4 to 6|
||Last hit: Damage decreased from 16 to 14|
||3rd hit: Hitstun reverted (decreased)|
|Neutral Jump Kick|Hitstop reverted (increased) from 8 to 12 (v5)|
|Back attack|OTG property added|
|Charge attack|Ability to use several charge attacks in a row restored (v4 feature)|
|Front Slam|Launch height increased|
|Back Slam / Suplex|Pushback decreased|||Launch height increased|
|Vanilla blitz (Hishousouzan)|Hitbox improved|||Launch height for airborne enemy increased|||Cancel window into air special made more lenient|

|Specials|Changes|
|---|---|
|Vanilla defensive (Embukyaku)|Hitbox data reverted (v5)|
||3rd hit: Cancellable into diagonal jump|
|Vanilla offensive (Kikou Shou)|Hitstop reverted (increased) to v7|
|Vanilla air (Tobi Kyaku)|Launch height reverted to v7 (airborne enemy)|
|Dive Kick (New move)|Can be performed after 1st or 2nd hit of air special (Down + Special Button)|
||Green Health cost set to 4|
||OTG property added|

### Cherry

|Basic moves|Changes|
|---|---|
|Auto-combo|Jab range increased|
||Last hit: Damage increased from 10 to 14|
||Last hit: Cancel into air special added
|Neutral jump kick|Hitstop increased|
||Hitbox improved (back part)|
||Launch height increased (airborne enemy)
|Down air|Ground Bounce added|
|Air kick forward|Hitbox slightly improved|
|Back attack|Launch height of airborne enemy increased
|Charge attack|I-frames removed|
||Damage increased from 24 to 26|
||Hitstop increased|
||Launch height increased (airborne enemy)|
||Cancel into neutral jump kick added|
||Cancel into air special added
|Charge attack (from air)|Launch height of airborne enemy increased
|“Headloops”|Head punches: Damage reduced|
||Slam: Launch height increased
|Back throw|Launch height increased|
|Vanilla blitz (Flying Knee)|Damage decreased from 32 to 26|
||Hitstops reverted (v07)|
||Cancellable into various jump attacks|
||Cancellable into charge attack|
||Launch height increased

|Specials|Changes|
|---|---|
|Vanilla defensive (Sound Check / Rock On)|Launch height reverted (v05)|
|Vanilla air (Air Townshend Smash)|Ground Bounce added|
|Vanilla star (Stage Entrance)|Hitboxes improved|
|Alt star (Triple Townshend)|Hitbox improved (forward and backward)|
||Pushback (1st, 2nd and 3rd hits) decreased|

### Floyd

|Basic moves|Changes|
|---|---|
|Down air|Damage increased from 6 to 8|
|Back attack|Launch height increased (airborne enemy)|
|Charge attack|Ability to use several charge attacks in a row restored (v4 feature)|
|Double Grab|Ground Bounce added|
|Jump Throw|Launch height increased|
|Vanilla blitz (Thunder Twins)|Launch height increased (airborne enemy)|2nd hit: Cancellable into Offensive Special|

|Specials|Changes|
|---|---|
|Vanilla defensive (Thunder Sphere)|Ignore weight property added|
||Hitbox improved|
||Launch height increased (airborne enemy)|
||Cancellable into offensive special|
||Decreased enemy fall speed|
|Vanilla offensive (Magnetic Grab / Gotcha Grab)|Faster animation|
||Hitbox improved|
||2nd hit: Damage increased from 1 to 16|
|Vanilla air (Thunder Pounce)|Directional variant: Launch height increased (airborne enemy)|

### Adam

|Basic moves|Changes|
|---|---|
|Auto-combo|Collision property after 4rd hit removed|
||Dash cancel added to 2nd, 3rd and 4th hit
|Forward Jump Kick|Hitbox improved|
|Back attack|Dash cancel added|
|Charge attack|Ability to do multiple charges in a row restored (v4 feature)|
||Dash cancel added|
|Back Throw|Throw distance restored (v07)|
|Back Slam / Suplex|Pushback decreased|Launch height increased|
|Vanilla blitz (Uzi Punch)|Damage decreased from 42 to 36|
||Pushback decreased (standing enemy)|
||2nd hit: Launch height increased (standing enemy)|
||2nd hit: Dash cancel added|
||OTG removed|

|Specials|Changes|
|---|---|
|Vanilla defensive (Chopper)|1st hit: Cancellable into Offensive Special|Ground Bounce added (all 3 hits)|
||Launch height increased (airborne enemy)|
|Vanilla offensive (Howl Fang)|Dash cancel added|
||2nd hit: Cancellable into Air Special|
|Vanilla air (Air Chopper)|Separated into 2 parts (press Special Button twice)|
||1st part – last hit: launches higher|
||1st part – last hit: less pushback (airborne enemies)|
||2nd part – last hit: Collision property removed|
||2nd part: I-Frames removed|
||2nd part: Ground Bounce added|
||2nd part: Cancellable into Neutral Jump Kick|

## DLC Characters (Highly experimental)

### Max

|Basic moves|Changes|
|---|---|
|Back attack|Launch height (1st hit) decreased|
|Charge attack|Damage increased from 22 to 26|
|Vanilla defensive (Spinning Knuckle Bomb)|Damage per hit decreased from 22 to 16|

### Estel

|Basic moves|Changes|
|---|---|
|Auto-combo & Neutral jump & Vanilla blitz (Boot Mark)|Hitstops reverted (v7)|
|Back attack|Damage slightly decreased from 16 to 12|
|Charge attack|I-frames removed|
|Vanilla offensive (Police Tackle)|Delay before final slam decreased|
||No longer consumes OTG

### Shiva

|Basic moves|Changes|
|---|---|
|Auto-combo|4th hit: Damage increased from 16 to 20|
||3rd hit: Pushback decreased|
|Back attack|Damage decreased from 20 to 16|
|Charge attack|Damage increased from 20 to 26|
|Target combo|Input change: needs to be performed with Back Attack + Charge Attack now|
|Vanilla blitz (Final Crash)|Damage decreased from 44 to 34|
||Increased enemy falling speed

|Specials|Changes|
|---|---|
|Vanilla offensive (Senretsu Ken)|Special variants separated and no longer depend on distance (press Special Button twice)|
||Close-range variant: hitbox improved|
|Vanilla air (Air Senretsu Ken)|I-frames on landing removed|

## I-Frame loop adjustments

Adjustments were made to the following moves in order to reduce the
effectiveness of I-Frame loops:

* Axel’s Alternate Blitz (Dragon Roll)
* Cherry’s Vanilla Blitz (Flying Knee) and Alternate Blitz (Sliding Knee)
* Floyd’s Alternate Blitz (Gatling Punches)
* Adam’s Alternate Blitz (Sword Upper)
* Max’s Alternate Blitz (Clothes Lining)
* Estel’s Vanilla Blitz (Boot Mark) and Alternate Blitz (Flying Scythe)
* Shiva’s Vanilla Blitz (Final Crash) and Alternate Blitz (Flying Kick)

They should still be possible, but require more precision to be executed.

## Older releases

Nothing here yet.
