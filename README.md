# Streets of Rage 4: REIGNITED

![SOR4-REIGNITED](./images/SOR4-Reignited.png)

Latest release: [SOR4: REIGNITED v1.0.0](./releaseNotes/index.md) <- *The mod download link is in here*

## About

The `Streets of Rage 4: REIGNITED` is a project aimed at re-balancing changes made to the game in it's latest patch (v8). In it's vanilla state, it feels like there is quite some room for improvement, which we hope to deliver with this passion project.

## Demo videos

[Trailer](https://youtu.be/MnnrE2JiqR4) - I'm not sure how to make embedded videos in markdown, sorry!

## Installation (PC only)

1\. Download the latest release (or past release of your choice)
2\. Navigate to your `Streets of Rage 4` installation directory, and go into the `..\data` folder

```powershell
<steam_install_path>\Steam\steamapps\common\Streets of Rage 4\data
```

3\. Back up the original `bigfile` (or just rename it)
4\. Extract the `bigfile` from the release archive, and replace the original
5\. Start the game, and enjoy!

### Development team

This mod was brought to you by:

* [FuzzYetDeadly](https://www.twitch.tv/fuzzyetdeadly)
* [MoonlightFox](https://www.youtube.com/@moonlightfox930)
* [Arkhlon_X](https://www.twitch.tv/arkhlon_x)
* [Rilsy](https://www.twitch.tv/rilsy)
* [Naddiwolf](https://www.twitch.tv/naddiwolf)

It was a collective effort of all of us to pool together ideas, implement them, and also further the development of the `SOR4 Editor` tool that was used to create it (no public release yet). I don't think this mod would have been possible had we not all banded together to work on it.

If you enjoyed our work, please drop by our channels some time to share your love and support 💖

The one thing we have in common, is that we're all Blaze mains across different versions of the game 😉

### Testers

The individuals outside the team who have helped trial run changes, or share general feedback with us at one point or another during closed beta testing. Thank you for your help!

* [Nexu14](https://www.twitch.tv/nexu14)
* [TheMightyBill](https://www.twitch.tv/themightybill)
* [Pato/DonMilitia](https://www.twitch.tv/patoeze/home)
* [KosoDragon](https://www.twitch.tv/kosodragon/home)

PS: If i missed anyone in the credits, please let me know

### Special thanks

* [JadingTsunami](https://github.com/JadingTsunami/bigfile_tools): he created a Python tool for editing `Bigfile`. He also taught me a few things to help me understand how to decrypt the contents of the file. This mod would not have been possible without his guidance.
* To `Dotemu` and `GuardCrush` for bringing us the timeless classic that is `Streets of Rage 4`. It's not easy creating a masterpiece, and they worked their very hardest to try and deliver an experience for the masses. While we may not be getting any new patches, there is nothing stopping us from creating our own now.

### Feedback

**I have some feedback I'd like to share. How can I do it?**

*Thank you for your interest in contributing towards improving the mod. If you have any feedback, kindly share it at the **comments section** in https://gamebanana.com/mods/473047, thanks!*
